FROM python:3.7.1-alpine3.7
RUN pip install cherrypy
ENV PORT=8080
WORKDIR /root
COPY code.py .
COPY index.html .
CMD ["python", "code.py"]
HEALTHCHECK CMD curl --fail http://localhost:8080 || exit 1
EXPOSE 8080

